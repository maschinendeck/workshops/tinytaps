<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_L" urn="urn:adsk.eagle:symbol:13867/1" library_version="1">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD" urn="urn:adsk.eagle:symbol:13864/1" library_version="1">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" urn="urn:adsk.eagle:component:13919/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="simon">
<packages>
<package name="SOIC8" urn="urn:adsk.eagle:footprint:4165/1" locally_modified="yes">
<description>&lt;B&gt;Wide Plastic Gull Wing Small Outline Package&lt;/B&gt;</description>
<smd name="1" x="-1.905" y="-3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="2" x="-0.645" y="-3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="3" x="0.625" y="-3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="4" x="1.895" y="-3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="3.154" dx="0.5" dy="2.2" layer="1"/>
<text x="-2.8575" y="-2.159" size="1.27" layer="25" font="vector" ratio="20" rot="R90">&gt;NAME</text>
<wire x1="-2.4" y1="1.8" x2="2.5" y2="1.8" width="0.254" layer="21"/>
<wire x1="2.5" y1="1.8" x2="2.5" y2="-1.8" width="0.254" layer="21"/>
<wire x1="2.5" y1="-1.8" x2="-2.4" y2="-1.8" width="0.254" layer="21"/>
<wire x1="-2.4" y1="-1.8" x2="-2.4" y2="1.8" width="0.254" layer="21"/>
<circle x="-1.7" y="-1.1" radius="0.3" width="0" layer="21"/>
<text x="4.2" y="-2.3" size="1.27" layer="27" font="vector" ratio="20" rot="R90">&gt;VALUE</text>
</package>
<package name="SOD323-R" urn="urn:adsk.eagle:footprint:43204/1" locally_modified="yes">
<description>&lt;b&gt;SOD323 Reflow soldering&lt;/b&gt; Philips SC01_Mounting_1996.pdf</description>
<wire x1="-1" y1="0.7" x2="1" y2="0.7" width="0.254" layer="21"/>
<wire x1="1" y1="-0.7" x2="-1" y2="-0.7" width="0.254" layer="21"/>
<wire x1="-0.5" y1="0" x2="0.1" y2="0.4" width="0.1524" layer="21"/>
<wire x1="0.1" y1="0.4" x2="0.1" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="0.1" y1="-0.4" x2="-0.5" y2="0" width="0.1524" layer="21"/>
<smd name="C" x="-1.1" y="0" dx="0.6" dy="0.6" layer="1"/>
<smd name="A" x="1.1" y="0" dx="0.6" dy="0.6" layer="1"/>
<wire x1="-0.508" y1="0.635" x2="-0.508" y2="-0.635" width="0.1524" layer="21"/>
<text x="-1.27" y="1.016" size="1.27" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.286" size="1.27" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="UX60A-MB-5ST" urn="urn:adsk.eagle:footprint:7171/1" locally_modified="yes">
<description>&lt;b&gt;MINI USB Connector HI-SPEED Certified&lt;/b&gt; Metal Shield SMT Type Without Positioning Post&lt;p&gt;
Source: http://www.hirose.co.jp/cataloge_hp/e24000019.pdf</description>
<smd name="M3" x="2" y="4.2" dx="2.2" dy="2.5" layer="1"/>
<smd name="M4" x="2" y="-4.2" dx="2.2" dy="2.5" layer="1"/>
<smd name="M2" x="-3.3" y="4.2" dx="2.2" dy="2.5" layer="1"/>
<smd name="M1" x="-3.3" y="-4.2" dx="2.2" dy="2.5" layer="1"/>
<smd name="1" x="2.25" y="1.6" dx="2" dy="0.5" layer="1"/>
<smd name="2" x="2.25" y="0.8" dx="2" dy="0.5" layer="1"/>
<smd name="3" x="2.25" y="0" dx="2" dy="0.5" layer="1"/>
<smd name="4" x="2.25" y="-0.8" dx="2" dy="0.5" layer="1"/>
<smd name="5" x="2.25" y="-1.6" dx="2" dy="0.5" layer="1"/>
<text x="-4.445" y="-6.985" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-6.4" y1="-3.85" x2="-4.425" y2="3.85" layer="41"/>
<rectangle x1="-2.175" y1="-3.85" x2="-0.45" y2="3.85" layer="41"/>
<rectangle x1="-4.425" y1="-2.925" x2="-2.175" y2="2.925" layer="41"/>
<rectangle x1="-0.45" y1="-1.85" x2="1.225" y2="1.85" layer="41"/>
<wire x1="-4.826" y1="3.81" x2="-4.826" y2="2.54" width="0.254" layer="21"/>
<wire x1="-4.826" y1="2.54" x2="-4.826" y2="-2.54" width="0.254" layer="21"/>
<wire x1="-4.826" y1="-2.54" x2="-4.826" y2="-3.81" width="0.254" layer="21"/>
<wire x1="-4.826" y1="-2.54" x2="3.81" y2="-2.54" width="0.254" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.254" layer="21"/>
<wire x1="3.81" y1="2.54" x2="-4.826" y2="2.54" width="0.254" layer="21"/>
<text x="5.588" y="-2.54" size="1.27" layer="25" font="vector" ratio="20" rot="R90">&gt;NAME</text>
<wire x1="-4.826" y1="3.81" x2="-6.096" y2="4.572" width="0.254" layer="25"/>
<wire x1="-6.096" y1="4.572" x2="-6.096" y2="-4.572" width="0.254" layer="25"/>
<wire x1="-6.096" y1="-4.572" x2="-4.826" y2="-3.81" width="0.254" layer="25"/>
<wire x1="-6.096" y1="4.572" x2="-4.826" y2="4.572" width="0.254" layer="25"/>
<wire x1="-6.096" y1="-4.572" x2="-4.826" y2="-4.572" width="0.254" layer="25"/>
<wire x1="-1.778" y1="-4.572" x2="0.508" y2="-4.572" width="0.254" layer="25"/>
<wire x1="-1.778" y1="4.572" x2="0.508" y2="4.572" width="0.254" layer="25"/>
</package>
<package name="UX60-MB-5ST" urn="urn:adsk.eagle:footprint:7173/1" locally_modified="yes">
<description>&lt;b&gt;MINI USB Connector HI-SPEED Certified&lt;/b&gt; Metal Shield SMT Type With Positioning Post&lt;p&gt;
Source: http://www.hirose.co.jp/cataloge_hp/e24000019.pdf</description>
<smd name="M3" x="2.6" y="4.3" dx="2.3" dy="2.2" layer="1"/>
<smd name="M4" x="2.6" y="-4.3" dx="2.3" dy="2.2" layer="1"/>
<smd name="M2" x="-2.9" y="4.3" dx="2.3" dy="2.2" layer="1"/>
<smd name="M1" x="-2.9" y="-4.3" dx="2.3" dy="2.2" layer="1"/>
<smd name="1" x="2.95" y="1.6" dx="1.6" dy="0.55" layer="1"/>
<smd name="2" x="2.95" y="0.8" dx="1.6" dy="0.55" layer="1"/>
<smd name="3" x="2.95" y="0" dx="1.6" dy="0.55" layer="1"/>
<smd name="4" x="2.95" y="-0.8" dx="1.6" dy="0.55" layer="1"/>
<smd name="5" x="2.95" y="-1.6" dx="1.6" dy="0.55" layer="1"/>
<text x="-4.445" y="-6.985" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="-2.2" drill="1"/>
<hole x="0" y="2.2" drill="1"/>
<text x="7.715" y="-3.9925" size="1.27" layer="25" font="vector" ratio="20" rot="R90">&gt;NAME</text>
<wire x1="3.4" y1="2.2" x2="3.4" y2="2.8" width="0.254" layer="25"/>
<wire x1="3.4" y1="-2.2" x2="3.4" y2="-2.8" width="0.254" layer="25"/>
<wire x1="1.2" y1="3.8" x2="-1.4" y2="3.8" width="0.254" layer="25"/>
<wire x1="1.2" y1="-3.8" x2="-1.4" y2="-3.8" width="0.254" layer="25"/>
<wire x1="-5.6" y1="3.8" x2="-5.6" y2="-3.8" width="0.254" layer="25"/>
<wire x1="-5.6" y1="-3.8" x2="-4.4" y2="-3.8" width="0.254" layer="25"/>
<wire x1="-4.4" y1="3.8" x2="-5.6" y2="3.8" width="0.254" layer="25"/>
<rectangle x1="-5.875" y1="-3.85" x2="-4.05" y2="3.85" layer="41"/>
</package>
<package name="WS2812B">
<smd name="VDD" x="-2" y="1.7" dx="2" dy="1.1" layer="1"/>
<smd name="DOUT" x="-2" y="-1.7" dx="2" dy="1.1" layer="1"/>
<smd name="VSS" x="2" y="-1.7" dx="2" dy="1.1" layer="1"/>
<smd name="DIN" x="2" y="1.7" dx="2" dy="1.1" layer="1"/>
<text x="-2" y="-4.1" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="-2.54" y="3.81" size="1.27" layer="21" font="vector" ratio="20">&gt;NAME</text>
<wire x1="-3.429" y1="3.429" x2="3.429" y2="3.429" width="0.254" layer="21"/>
<wire x1="-3.429" y1="-3.429" x2="-3.429" y2="3.429" width="0.254" layer="21"/>
<wire x1="-3.429" y1="-3.429" x2="2.54" y2="-3.429" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.429" x2="3.429" y2="-2.54" width="0.254" layer="21"/>
<wire x1="3.429" y1="-2.54" x2="3.429" y2="3.429" width="0.254" layer="21"/>
</package>
<package name="DTSM-6">
<wire x1="-3.1" y1="3.1" x2="3.1" y2="3.1" width="0.254" layer="51"/>
<wire x1="3.1" y1="3.1" x2="3.1" y2="-3.1" width="0.254" layer="51"/>
<wire x1="3.1" y1="-3.1" x2="-3.1" y2="-3.1" width="0.254" layer="51"/>
<wire x1="-3.1" y1="-3.1" x2="-3.1" y2="3.1" width="0.254" layer="51"/>
<wire x1="2.75" y1="3.1" x2="-2.75" y2="3.1" width="0.254" layer="21"/>
<wire x1="3.1" y1="-1" x2="3.1" y2="1" width="0.254" layer="21"/>
<wire x1="2.75" y1="-3.1" x2="-2.75" y2="-3.1" width="0.254" layer="21"/>
<wire x1="-3.1" y1="-1" x2="-3.1" y2="1" width="0.254" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="51"/>
<circle x="0" y="0" radius="1.75" width="0.254" layer="21"/>
<smd name="1" x="-3.975" y="2.25" dx="1.55" dy="1.3" layer="1"/>
<smd name="2" x="3.975" y="2.25" dx="1.55" dy="1.3" layer="1"/>
<smd name="3" x="-3.975" y="-2.25" dx="1.55" dy="1.3" layer="1"/>
<smd name="4" x="3.975" y="-2.25" dx="1.55" dy="1.3" layer="1"/>
<text x="-2.54" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.54" y="3.81" size="1.27" layer="21" font="vector" ratio="20">&gt;NAME</text>
</package>
<package name="R0603" urn="urn:adsk.eagle:footprint:23044/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.856" x2="1.473" y2="0.856" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.856" x2="1.473" y2="-0.856" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.856" x2="-1.473" y2="-0.856" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.856" x2="-1.473" y2="0.856" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.524" y1="0.889" x2="1.524" y2="0.889" width="0.254" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.524" y2="-0.889" width="0.254" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="-1.524" y2="-0.889" width="0.254" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.524" y2="0.889" width="0.254" layer="21"/>
<text x="-1.524" y="1.27" size="1.27" layer="21" font="vector" ratio="20">&gt;NAME</text>
</package>
<package name="CT/CN1206" urn="urn:adsk.eagle:footprint:30510/1" locally_modified="yes" library_version="3">
<description>&lt;b&gt;EPCOS SMD Varistors, MLV; Standard Series&lt;/b&gt;&lt;p&gt;
Source: www.farnell.com/datasheets/49238.pdf</description>
<smd name="1" x="-1.45" y="0" dx="1.2" dy="1.8" layer="1"/>
<smd name="2" x="1.45" y="0" dx="1.2" dy="1.8" layer="1"/>
<text x="-2" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.794" y="1.778" size="1.27" layer="25" font="vector" ratio="20">&gt;NAME</text>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
</package>
<package name="8XPINHEAD">
<pad name="1" x="-8.89" y="0" drill="1" diameter="1.8"/>
<pad name="2" x="-6.35" y="0" drill="1" diameter="1.8"/>
<pad name="3" x="-3.81" y="0" drill="1" diameter="1.8"/>
<wire x1="-10.16" y1="1.27" x2="-7.62" y2="1.27" width="0.254" layer="21"/>
<wire x1="-7.62" y1="1.27" x2="10.16" y2="1.27" width="0.254" layer="21"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.254" layer="21"/>
<wire x1="10.16" y1="-1.27" x2="-7.62" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-7.62" y1="-1.27" x2="-10.16" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-10.16" y1="-1.27" x2="-10.16" y2="1.27" width="0.254" layer="21"/>
<wire x1="-7.62" y1="1.27" x2="-7.62" y2="-1.27" width="0.254" layer="21"/>
<text x="-10.16" y="1.778" size="1.27" layer="25" font="vector" ratio="20">&gt;NAME</text>
<pad name="4" x="-1.27" y="0" drill="1" diameter="1.8"/>
<pad name="5" x="1.27" y="0" drill="1" diameter="1.8"/>
<pad name="6" x="3.81" y="0" drill="1" diameter="1.8"/>
<pad name="7" x="6.35" y="0" drill="1" diameter="1.8"/>
<pad name="8" x="8.89" y="0" drill="1" diameter="1.8"/>
</package>
<package name="3XPINHEAD">
<pad name="1" x="-2.54" y="0" drill="1" diameter="1.8"/>
<pad name="2" x="0" y="0" drill="1" diameter="1.8"/>
<pad name="3" x="2.54" y="0" drill="1" diameter="1.8"/>
<wire x1="-3.81" y1="1.27" x2="-1.27" y2="1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.254" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-3.81" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="21"/>
<text x="-3.81" y="1.778" size="1.27" layer="25" font="vector" ratio="20">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="UX60A-MB-5ST" urn="urn:adsk.eagle:package:7263/1" type="box">
<description>MINI USB Connector HI-SPEED Certified Metal Shield SMT Type Without Positioning Post
Source: http://www.hirose.co.jp/cataloge_hp/e24000019.pdf</description>
<packageinstances>
<packageinstance name="UX60A-MB-5ST"/>
</packageinstances>
</package3d>
<package3d name="UX60-MB-5ST" urn="urn:adsk.eagle:package:7266/1" type="box">
<description>MINI USB Connector HI-SPEED Certified Metal Shield SMT Type With Positioning Post
Source: http://www.hirose.co.jp/cataloge_hp/e24000019.pdf</description>
<packageinstances>
<packageinstance name="UX60-MB-5ST"/>
</packageinstances>
</package3d>
<package3d name="R0603" urn="urn:adsk.eagle:package:23555/3" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0603"/>
</packageinstances>
</package3d>
<package3d name="CT/CN1206" urn="urn:adsk.eagle:package:30599/1" type="box" library_version="3">
<description>EPCOS SMD Varistors, MLV; Standard Series
Source: www.farnell.com/datasheets/49238.pdf</description>
<packageinstances>
<packageinstance name="CT/CN1206"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ATTINY85">
<pin name="GND" x="-12.7" y="-5.08" length="short"/>
<pin name="VCC" x="-12.7" y="7.62" length="short"/>
<pin name="PB5" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="PB0" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="PB1" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="PB2" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="PB3" x="12.7" y="0" length="short" rot="R180"/>
<pin name="PB4" x="12.7" y="-2.54" length="short" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="11.43" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-10.16" y="-10.16" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="D-ZENER">
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.397" y1="-1.905" x2="2.032" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.905" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="-2.9464" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.4704" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
<symbol name="MINI-USB">
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-1.27" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="1.016" y2="-8.128" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="1.016" y1="-8.128" x2="2.54" y2="-8.89" width="0.254" layer="94" curve="53.130102"/>
<wire x1="2.54" y1="-8.89" x2="5.08" y2="-8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="-8.89" x2="6.35" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-1.27" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="1.016" y2="8.128" width="0.254" layer="94" curve="53.130102"/>
<wire x1="1.016" y1="8.128" x2="2.54" y2="8.89" width="0.254" layer="94" curve="-53.130102"/>
<wire x1="2.54" y1="8.89" x2="5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="8.89" x2="6.35" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="3.81" y2="-6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="-6.35" x2="3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="6.35" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="11.43" size="1.778" layer="95">&gt;NAME</text>
<text x="10.16" y="-7.62" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="1" x="-5.08" y="5.08" visible="pin" direction="in"/>
<pin name="2" x="-5.08" y="2.54" visible="pin" direction="in"/>
<pin name="3" x="-5.08" y="0" visible="pin" direction="in"/>
<pin name="4" x="-5.08" y="-2.54" visible="pin" direction="in"/>
<pin name="5" x="-5.08" y="-5.08" visible="pin" direction="in"/>
</symbol>
<symbol name="WS2812B">
<pin name="DI" x="-12.7" y="-2.54" visible="pad" length="middle" direction="in"/>
<pin name="GND" x="0" y="-10.16" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="DO" x="12.7" y="-2.54" visible="pad" length="middle" direction="out" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-6.35" y1="6.35" x2="-5.08" y2="6.35" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-6.35" y2="6.35" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="-6.35" y1="3.81" x2="-5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<pin name="VDD" x="0" y="15.24" visible="pad" length="middle" direction="pwr" rot="R270"/>
<wire x1="0" y1="10.16" x2="0" y2="8.89" width="0.254" layer="94"/>
<wire x1="0" y1="8.89" x2="-5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="-5.08" y1="8.89" x2="-5.08" y2="6.35" width="0.254" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="-1.27" y2="8.89" width="0.254" layer="94"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="8.89" width="0.254" layer="94"/>
<wire x1="2.54" y1="8.89" x2="0" y2="8.89" width="0.254" layer="94"/>
<wire x1="-5.08" y1="6.35" x2="-3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="-5.08" y1="3.81" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="-7.62" y2="-2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="5.08" y1="-1.27" x2="7.62" y2="-2.54" width="0.254" layer="94" style="shortdash"/>
<wire x1="0" y1="-1.27" x2="0" y2="-5.08" width="0.254" layer="94"/>
<text x="3.81" y="1.27" size="1.27" layer="94" rot="R180">WS2812B</text>
<wire x1="-3.81" y1="3.81" x2="-5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="0" y2="6.35" width="0.254" layer="94"/>
<wire x1="0" y1="6.35" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="8.89" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="8.89" x2="5.08" y2="8.89" width="0.254" layer="94"/>
</symbol>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="1" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="3" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="4" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="VARISTOR">
<wire x1="2.54" y1="1.016" x2="2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.016" x2="-2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.016" x2="-2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="2.032" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-2.032" y1="2.032" x2="-2.54" y2="2.032" width="0.254" layer="94"/>
<wire x1="2.032" y1="-2.032" x2="-2.032" y2="2.032" width="0.254" layer="94"/>
<text x="-2.54" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="PINHD8">
<wire x1="-6.35" y1="-10.16" x2="1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD3">
<wire x1="-6.35" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATTINY85" prefix="IC">
<gates>
<gate name="G$1" symbol="ATTINY85" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC8">
<connects>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="PB0" pad="5"/>
<connect gate="G$1" pin="PB1" pad="6"/>
<connect gate="G$1" pin="PB2" pad="7"/>
<connect gate="G$1" pin="PB3" pad="2"/>
<connect gate="G$1" pin="PB4" pad="3"/>
<connect gate="G$1" pin="PB5" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZD" prefix="ZD">
<gates>
<gate name="G$1" symbol="D-ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD323-R">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MINI-USB-" prefix="X">
<description>&lt;b&gt;MINI USB Connector&lt;/b&gt;&lt;p&gt;
Source: http://www.hirose.co.jp/cataloge_hp/e24000019.pdf</description>
<gates>
<gate name="G$1" symbol="MINI-USB" x="0" y="0"/>
</gates>
<devices>
<device name="UX60A-MB-5ST" package="UX60A-MB-5ST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7263/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="UX60-MB-5ST" package="UX60-MB-5ST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7266/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WS2812B" prefix="LED">
<description>Features and Benefits

* Intelligent reverse connect protection, the power supply reverse connection does not damage the IC.

* The control circuit and the LED share the only power source.
Control circuit and RGB chip are integrated in a package of 5050 components, form a complete control of pixel point.

* Built-in signal reshaping circuit, after wave reshaping to the next driver, ensure wave-form distortion not accumulate.

* Built-in electric reset circuit and power lost reset circuit.

* Each pixel of the three primary color can achieve 256 brightness display, completed 16777216 color full color display, and scan frequency not less than 400Hz/s.

* Cascading port transmission signal by single line.

* Any two point the distance more than 5m transmission signal without any increase circuit.

* When the refresh rate is 30fps, cascade number are not less than1024 points.

* Send data at speeds of 800Kbps.

*The color of the light were highly consistent, cost-effective.



Applications

Full-color module, Full color soft lights a lamp strip.
LED decorative lighting, Indoor/outdoor LED video irregular screen.


General description

WS2812B is a intelligent control LED light source that the control circuit and RGB chip are integrated in a package of 5050 components. It internal include intelligent digital port data latch and signal reshaping amplification drive circuit. Also include a precision internal oscillator and a 12V voltage programmable constant curre-nt control part, effectively ensuring the pixel point light color height consistent.
The data transfer protocol use single NZR communication mode. After the pixel power-on reset, the DIN port receive data from controller, the first pixel collect initial 24bit data then sent to the internal data latch, the other data which reshaping by the internal signal reshaping amplification circuit sent to the next cascade pixel through the DO port. After transmission for each pixel,the signal to reduce 24bit. pixel adopt auto reshaping transmit technology, making the pixel cascade number is not limited the signal transmission, only depend on the speed of signal transmission.
LED with low driving voltage, environmental protection and energy saving, high brightness, scattering angle is large, good consistency, low power, long life and other advantages. The control chip integrated in LED above becoming more simple circuit, small volume, convenient installation.


Absolute Maximum Ratings
Power supply voltage:  VDD  +3.5V..+5.3V
Input voltage:  VI -0.5V..VDD+0.5V



based on WS2812 from https://github.com/adafruit/Adafruit-Eagle-Library</description>
<gates>
<gate name="G$1" symbol="WS2812B" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="WS2812B">
<connects>
<connect gate="G$1" pin="DI" pad="DIN"/>
<connect gate="G$1" pin="DO" pad="DOUT"/>
<connect gate="G$1" pin="GND" pad="VSS"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DTSM-6" prefix="S">
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DTSM-6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="70" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="R">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
<deviceset name="POLYFUSE" prefix="F" uservalue="yes">
<description>&lt;b&gt;VARISTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VARISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="CN1206" package="CT/CN1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30599/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD8" prefix="X">
<gates>
<gate name="G$1" symbol="PINHD8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="8XPINHEAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD3" prefix="X">
<gates>
<gate name="G$1" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="3XPINHEAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="000_eigene_Bauteile">
<packages>
<package name="BOCMARK">
<circle x="0" y="0" radius="0.5" width="0" layer="1"/>
<circle x="0" y="0" radius="1.5" width="0" layer="39"/>
<circle x="0" y="0" radius="1.5" width="0" layer="41"/>
<circle x="0" y="0" radius="1.5" width="0" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="BOC_MARK">
<circle x="0" y="0" radius="3.5921" width="0.254" layer="94"/>
<circle x="0" y="0" radius="1.36783125" width="0" layer="94"/>
<wire x1="-4.318" y1="4.318" x2="4.318" y2="4.318" width="0.254" layer="94"/>
<wire x1="4.318" y1="4.318" x2="4.318" y2="-4.318" width="0.254" layer="94"/>
<wire x1="4.318" y1="-4.318" x2="-4.318" y2="-4.318" width="0.254" layer="94"/>
<wire x1="-4.318" y1="-4.318" x2="-4.318" y2="4.318" width="0.254" layer="94"/>
<text x="-4.318" y="4.826" size="1.778" layer="95" font="vector">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BOCMARK" prefix="BOC">
<gates>
<gate name="G$1" symbol="BOC_MARK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BOCMARK">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TinyTaps">
<packages>
<package name="C0603" urn="urn:adsk.eagle:footprint:23123/1" locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.524" y1="0.889" x2="1.524" y2="0.889" width="0.254" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.524" y2="-0.889" width="0.254" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="-1.524" y2="-0.889" width="0.254" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.524" y2="0.889" width="0.254" layer="21"/>
<text x="-1.27" y="1.27" size="1.27" layer="25" font="vector" ratio="20">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-EU" prefix="C">
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="X3" library="simon" deviceset="MINI-USB-" device="UX60-MB-5ST" package3d_urn="urn:adsk.eagle:package:7266/1" value="MINI-USB-UX60-MB-5ST"/>
<part name="R1" library="simon" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="68R"/>
<part name="R2" library="simon" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="68R"/>
<part name="R3" library="simon" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k5"/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="F1" library="simon" deviceset="POLYFUSE" device="CN1206" package3d_urn="urn:adsk.eagle:package:30599/1" value="500mA Polyfuse"/>
<part name="C1" library="TinyTaps" deviceset="C-EU" device="" value="4u7"/>
<part name="C2" library="TinyTaps" deviceset="C-EU" device="" value="100n"/>
<part name="C3" library="TinyTaps" deviceset="C-EU" device="" value="100n"/>
<part name="C4" library="TinyTaps" deviceset="C-EU" device="" value="100n"/>
<part name="C5" library="TinyTaps" deviceset="C-EU" device="" value="100n"/>
<part name="C6" library="TinyTaps" deviceset="C-EU" device="" value="100n"/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC2" library="simon" deviceset="ATTINY85" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="ZD1" library="simon" deviceset="ZD" device="" value="3V3"/>
<part name="ZD2" library="simon" deviceset="ZD" device="" value="3V3"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R5" library="simon" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k5"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="R6" library="simon" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k5"/>
<part name="R7" library="simon" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k5"/>
<part name="R8" library="simon" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k5"/>
<part name="S2" library="simon" deviceset="DTSM-6" device=""/>
<part name="S1" library="simon" deviceset="DTSM-6" device=""/>
<part name="S4" library="simon" deviceset="DTSM-6" device=""/>
<part name="S3" library="simon" deviceset="DTSM-6" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="LED2" library="simon" deviceset="WS2812B" device=""/>
<part name="LED1" library="simon" deviceset="WS2812B" device=""/>
<part name="LED4" library="simon" deviceset="WS2812B" device=""/>
<part name="LED3" library="simon" deviceset="WS2812B" device=""/>
<part name="R9" library="simon" deviceset="R-EU_" device="R0603" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k5"/>
<part name="X1" library="simon" deviceset="PINHEAD3" device=""/>
<part name="X2" library="simon" deviceset="PINHEAD8" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="BOC1" library="000_eigene_Bauteile" deviceset="BOCMARK" device=""/>
<part name="BOC2" library="000_eigene_Bauteile" deviceset="BOCMARK" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="GND1" gate="1" x="33.02" y="127" smashed="yes">
<attribute name="VALUE" x="30.48" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="1" x="33.02" y="167.64" smashed="yes">
<attribute name="VALUE" x="30.48" y="162.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X3" gate="G$1" x="25.4" y="142.24" smashed="yes" rot="MR0">
<attribute name="NAME" x="27.94" y="153.67" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="15.24" y="134.62" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="R1" gate="G$1" x="53.34" y="144.78" smashed="yes">
<attribute name="NAME" x="49.53" y="146.2786" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="141.478" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="63.5" y="142.24" smashed="yes">
<attribute name="NAME" x="59.69" y="143.7386" size="1.778" layer="95"/>
<attribute name="VALUE" x="59.69" y="138.938" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="45.72" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="44.2214" y="151.13" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="49.022" y="151.13" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+3" gate="1" x="45.72" y="167.64" smashed="yes">
<attribute name="VALUE" x="43.18" y="162.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="F1" gate="G$1" x="33.02" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="29.21" y="152.4" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="36.83" y="149.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C1" gate="G$1" x="96.52" y="147.32" smashed="yes">
<attribute name="NAME" x="98.044" y="147.701" size="1.778" layer="95"/>
<attribute name="VALUE" x="98.044" y="142.621" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="104.14" y="147.32" smashed="yes">
<attribute name="NAME" x="105.664" y="147.701" size="1.778" layer="95"/>
<attribute name="VALUE" x="105.664" y="142.621" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="111.76" y="147.32" smashed="yes">
<attribute name="NAME" x="113.284" y="147.701" size="1.778" layer="95"/>
<attribute name="VALUE" x="113.284" y="142.621" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="119.38" y="147.32" smashed="yes">
<attribute name="NAME" x="120.904" y="147.701" size="1.778" layer="95"/>
<attribute name="VALUE" x="120.904" y="142.621" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="127" y="147.32" smashed="yes">
<attribute name="NAME" x="128.524" y="147.701" size="1.778" layer="95"/>
<attribute name="VALUE" x="128.524" y="142.621" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="134.62" y="147.32" smashed="yes">
<attribute name="NAME" x="136.144" y="147.701" size="1.778" layer="95"/>
<attribute name="VALUE" x="136.144" y="142.621" size="1.778" layer="96"/>
</instance>
<instance part="P+4" gate="1" x="96.52" y="157.48" smashed="yes">
<attribute name="VALUE" x="93.98" y="152.4" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND2" gate="1" x="96.52" y="134.62" smashed="yes">
<attribute name="VALUE" x="93.98" y="132.08" size="1.778" layer="96"/>
</instance>
<instance part="IC2" gate="G$1" x="40.64" y="71.12" smashed="yes">
<attribute name="NAME" x="30.48" y="82.55" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="30.48" y="60.96" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="P+5" gate="1" x="25.4" y="106.68" smashed="yes">
<attribute name="VALUE" x="22.86" y="101.6" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="1" x="25.4" y="55.88" smashed="yes">
<attribute name="VALUE" x="22.86" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="ZD1" gate="G$1" x="45.72" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="43.0784" y="131.6736" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="50.2158" y="130.1496" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="ZD2" gate="G$1" x="55.88" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="53.2384" y="131.6736" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="60.3758" y="130.1496" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND4" gate="1" x="45.72" y="127" smashed="yes">
<attribute name="VALUE" x="43.18" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="55.88" y="127" smashed="yes">
<attribute name="VALUE" x="53.34" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="88.9" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="87.4014" y="82.55" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="92.202" y="82.55" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+2" gate="1" x="88.9" y="106.68" smashed="yes">
<attribute name="VALUE" x="86.36" y="101.6" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R6" gate="G$1" x="88.9" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="87.4014" y="49.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="92.202" y="49.53" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R7" gate="G$1" x="88.9" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="87.4014" y="39.37" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="92.202" y="39.37" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R8" gate="G$1" x="88.9" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="87.4014" y="29.21" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="92.202" y="29.21" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S2" gate="G$1" x="60.96" y="22.86" smashed="yes">
<attribute name="NAME" x="54.61" y="20.32" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="57.15" y="26.035" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S1" gate="G$1" x="48.26" y="22.86" smashed="yes">
<attribute name="NAME" x="41.91" y="20.32" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="44.45" y="26.035" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S4" gate="G$1" x="86.36" y="22.86" smashed="yes">
<attribute name="NAME" x="80.01" y="20.32" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="82.55" y="26.035" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S3" gate="G$1" x="73.66" y="22.86" smashed="yes">
<attribute name="NAME" x="67.31" y="20.32" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="69.85" y="26.035" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND6" gate="1" x="48.26" y="12.7" smashed="yes">
<attribute name="VALUE" x="45.72" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="LED2" gate="G$1" x="139.7" y="78.74" smashed="yes"/>
<instance part="LED1" gate="G$1" x="106.68" y="78.74" smashed="yes"/>
<instance part="LED4" gate="G$1" x="205.74" y="78.74" smashed="yes"/>
<instance part="LED3" gate="G$1" x="172.72" y="78.74" smashed="yes"/>
<instance part="R9" gate="G$1" x="231.14" y="76.2" smashed="yes" rot="R180">
<attribute name="NAME" x="234.95" y="74.7014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="234.95" y="79.502" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="X1" gate="G$1" x="248.92" y="76.2" smashed="yes">
<attribute name="NAME" x="242.57" y="81.915" size="1.778" layer="95"/>
<attribute name="VALUE" x="242.57" y="68.58" size="1.778" layer="96"/>
</instance>
<instance part="X2" gate="G$1" x="66.04" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="52.705" y="102.87" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="78.74" y="102.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND7" gate="1" x="73.66" y="91.44" smashed="yes">
<attribute name="VALUE" x="71.12" y="88.9" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="106.68" y="60.96" smashed="yes">
<attribute name="VALUE" x="104.14" y="58.42" size="1.778" layer="96"/>
</instance>
<instance part="BOC1" gate="G$1" x="121.92" y="12.7" smashed="yes">
<attribute name="NAME" x="117.602" y="17.526" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="BOC2" gate="G$1" x="134.62" y="12.7" smashed="yes">
<attribute name="NAME" x="130.302" y="17.526" size="1.778" layer="95" font="vector"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+5V" class="0">
<segment>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="45.72" y1="165.1" x2="45.72" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+1" gate="1" pin="+5V"/>
<pinref part="F1" gate="G$1" pin="1"/>
<wire x1="33.02" y1="165.1" x2="33.02" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="134.62" y1="149.86" x2="134.62" y2="152.4" width="0.1524" layer="91"/>
<wire x1="134.62" y1="152.4" x2="127" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="127" y1="152.4" x2="127" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="119.38" y1="149.86" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<wire x1="119.38" y1="152.4" x2="127" y2="152.4" width="0.1524" layer="91"/>
<junction x="127" y="152.4"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="111.76" y1="149.86" x2="111.76" y2="152.4" width="0.1524" layer="91"/>
<wire x1="111.76" y1="152.4" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<junction x="119.38" y="152.4"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="104.14" y1="149.86" x2="104.14" y2="152.4" width="0.1524" layer="91"/>
<wire x1="104.14" y1="152.4" x2="111.76" y2="152.4" width="0.1524" layer="91"/>
<junction x="111.76" y="152.4"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="96.52" y1="149.86" x2="96.52" y2="152.4" width="0.1524" layer="91"/>
<wire x1="96.52" y1="152.4" x2="104.14" y2="152.4" width="0.1524" layer="91"/>
<junction x="104.14" y="152.4"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="96.52" y1="154.94" x2="96.52" y2="152.4" width="0.1524" layer="91"/>
<junction x="96.52" y="152.4"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="27.94" y1="78.74" x2="25.4" y2="78.74" width="0.1524" layer="91"/>
<wire x1="25.4" y1="78.74" x2="25.4" y2="96.52" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="7"/>
<wire x1="25.4" y1="96.52" x2="25.4" y2="104.14" width="0.1524" layer="91"/>
<wire x1="71.12" y1="106.68" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
<wire x1="71.12" y1="96.52" x2="25.4" y2="96.52" width="0.1524" layer="91"/>
<junction x="25.4" y="96.52"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="88.9" y1="104.14" x2="88.9" y2="96.52" width="0.1524" layer="91"/>
<wire x1="88.9" y1="96.52" x2="88.9" y2="91.44" width="0.1524" layer="91"/>
<wire x1="88.9" y1="96.52" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<wire x1="106.68" y1="96.52" x2="139.7" y2="96.52" width="0.1524" layer="91"/>
<wire x1="139.7" y1="96.52" x2="172.72" y2="96.52" width="0.1524" layer="91"/>
<wire x1="172.72" y1="96.52" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<wire x1="205.74" y1="96.52" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
<wire x1="241.3" y1="96.52" x2="241.3" y2="78.74" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="241.3" y1="78.74" x2="246.38" y2="78.74" width="0.1524" layer="91"/>
<junction x="88.9" y="96.52"/>
<pinref part="LED1" gate="G$1" pin="VDD"/>
<wire x1="106.68" y1="93.98" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<junction x="106.68" y="96.52"/>
<pinref part="LED2" gate="G$1" pin="VDD"/>
<wire x1="139.7" y1="93.98" x2="139.7" y2="96.52" width="0.1524" layer="91"/>
<junction x="139.7" y="96.52"/>
<pinref part="LED3" gate="G$1" pin="VDD"/>
<wire x1="172.72" y1="93.98" x2="172.72" y2="96.52" width="0.1524" layer="91"/>
<junction x="172.72" y="96.52"/>
<pinref part="LED4" gate="G$1" pin="VDD"/>
<wire x1="205.74" y1="93.98" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<junction x="205.74" y="96.52"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="X3" gate="G$1" pin="5"/>
<wire x1="30.48" y1="137.16" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="33.02" y1="137.16" x2="33.02" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="96.52" y1="137.16" x2="96.52" y2="139.7" width="0.1524" layer="91"/>
<wire x1="96.52" y1="139.7" x2="96.52" y2="142.24" width="0.1524" layer="91"/>
<wire x1="96.52" y1="139.7" x2="104.14" y2="139.7" width="0.1524" layer="91"/>
<junction x="96.52" y="139.7"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="104.14" y1="139.7" x2="104.14" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="111.76" y1="142.24" x2="111.76" y2="139.7" width="0.1524" layer="91"/>
<wire x1="111.76" y1="139.7" x2="104.14" y2="139.7" width="0.1524" layer="91"/>
<junction x="104.14" y="139.7"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="119.38" y1="142.24" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<wire x1="119.38" y1="139.7" x2="111.76" y2="139.7" width="0.1524" layer="91"/>
<junction x="111.76" y="139.7"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="127" y1="142.24" x2="127" y2="139.7" width="0.1524" layer="91"/>
<wire x1="127" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<junction x="119.38" y="139.7"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="134.62" y1="142.24" x2="134.62" y2="139.7" width="0.1524" layer="91"/>
<wire x1="134.62" y1="139.7" x2="127" y2="139.7" width="0.1524" layer="91"/>
<junction x="127" y="139.7"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="27.94" y1="66.04" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<wire x1="25.4" y1="66.04" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ZD1" gate="G$1" pin="A"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="45.72" y1="132.08" x2="45.72" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="ZD2" gate="G$1" pin="A"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="55.88" y1="132.08" x2="55.88" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="8"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="73.66" y1="106.68" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="106.68" y1="66.04" x2="106.68" y2="63.5" width="0.1524" layer="91"/>
<wire x1="106.68" y1="66.04" x2="139.7" y2="66.04" width="0.1524" layer="91"/>
<wire x1="139.7" y1="66.04" x2="172.72" y2="66.04" width="0.1524" layer="91"/>
<wire x1="172.72" y1="66.04" x2="205.74" y2="66.04" width="0.1524" layer="91"/>
<wire x1="205.74" y1="66.04" x2="241.3" y2="66.04" width="0.1524" layer="91"/>
<wire x1="241.3" y1="66.04" x2="241.3" y2="73.66" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="3"/>
<wire x1="241.3" y1="73.66" x2="246.38" y2="73.66" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="68.58" x2="106.68" y2="66.04" width="0.1524" layer="91"/>
<junction x="106.68" y="66.04"/>
<pinref part="LED2" gate="G$1" pin="GND"/>
<wire x1="139.7" y1="68.58" x2="139.7" y2="66.04" width="0.1524" layer="91"/>
<junction x="139.7" y="66.04"/>
<pinref part="LED3" gate="G$1" pin="GND"/>
<wire x1="172.72" y1="68.58" x2="172.72" y2="66.04" width="0.1524" layer="91"/>
<junction x="172.72" y="66.04"/>
<pinref part="LED4" gate="G$1" pin="GND"/>
<wire x1="205.74" y1="68.58" x2="205.74" y2="66.04" width="0.1524" layer="91"/>
<junction x="205.74" y="66.04"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="48.26" y1="15.24" x2="48.26" y2="17.78" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="48.26" y1="17.78" x2="50.8" y2="17.78" width="0.1524" layer="91"/>
<junction x="48.26" y="17.78"/>
<pinref part="S2" gate="G$1" pin="1"/>
<wire x1="50.8" y1="17.78" x2="60.96" y2="17.78" width="0.1524" layer="91"/>
<junction x="50.8" y="17.78"/>
<pinref part="S2" gate="G$1" pin="2"/>
<wire x1="60.96" y1="17.78" x2="63.5" y2="17.78" width="0.1524" layer="91"/>
<junction x="60.96" y="17.78"/>
<pinref part="S3" gate="G$1" pin="2"/>
<pinref part="S3" gate="G$1" pin="1"/>
<wire x1="73.66" y1="17.78" x2="76.2" y2="17.78" width="0.1524" layer="91"/>
<wire x1="63.5" y1="17.78" x2="73.66" y2="17.78" width="0.1524" layer="91"/>
<junction x="63.5" y="17.78"/>
<junction x="73.66" y="17.78"/>
<pinref part="S4" gate="G$1" pin="2"/>
<pinref part="S4" gate="G$1" pin="1"/>
<wire x1="86.36" y1="17.78" x2="88.9" y2="17.78" width="0.1524" layer="91"/>
<wire x1="76.2" y1="17.78" x2="86.36" y2="17.78" width="0.1524" layer="91"/>
<junction x="76.2" y="17.78"/>
<junction x="86.36" y="17.78"/>
</segment>
</net>
<net name="USB_D-" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="71.12" y1="144.78" x2="58.42" y2="144.78" width="0.1524" layer="91"/>
<label x="71.12" y="144.78" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="4"/>
<pinref part="IC2" gate="G$1" pin="PB3"/>
<wire x1="63.5" y1="106.68" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
<wire x1="63.5" y1="71.12" x2="53.34" y2="71.12" width="0.1524" layer="91"/>
<wire x1="71.12" y1="71.12" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
<junction x="63.5" y="71.12"/>
<label x="71.12" y="71.12" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="X3" gate="G$1" pin="3"/>
<wire x1="58.42" y1="142.24" x2="55.88" y2="142.24" width="0.1524" layer="91"/>
<pinref part="ZD2" gate="G$1" pin="C"/>
<wire x1="55.88" y1="142.24" x2="30.48" y2="142.24" width="0.1524" layer="91"/>
<wire x1="55.88" y1="142.24" x2="55.88" y2="137.16" width="0.1524" layer="91"/>
<junction x="55.88" y="142.24"/>
</segment>
</net>
<net name="USB_D+" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="71.12" y1="142.24" x2="68.58" y2="142.24" width="0.1524" layer="91"/>
<label x="71.12" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="5"/>
<pinref part="IC2" gate="G$1" pin="PB4"/>
<wire x1="66.04" y1="106.68" x2="66.04" y2="68.58" width="0.1524" layer="91"/>
<wire x1="66.04" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="71.12" y1="68.58" x2="66.04" y2="68.58" width="0.1524" layer="91"/>
<junction x="66.04" y="68.58"/>
<label x="71.12" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="X3" gate="G$1" pin="2"/>
<wire x1="30.48" y1="144.78" x2="45.72" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="45.72" y1="144.78" x2="45.72" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="45.72" y1="144.78" x2="48.26" y2="144.78" width="0.1524" layer="91"/>
<junction x="45.72" y="144.78"/>
<pinref part="ZD1" gate="G$1" pin="C"/>
<wire x1="45.72" y1="144.78" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="X3" gate="G$1" pin="1"/>
<wire x1="30.48" y1="147.32" x2="33.02" y2="147.32" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="2"/>
<wire x1="33.02" y1="149.86" x2="33.02" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="76.2" y1="38.1" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<junction x="88.9" y="38.1"/>
<pinref part="S3" gate="G$1" pin="4"/>
<pinref part="S3" gate="G$1" pin="3"/>
<wire x1="73.66" y1="27.94" x2="76.2" y2="27.94" width="0.1524" layer="91"/>
<wire x1="76.2" y1="38.1" x2="76.2" y2="27.94" width="0.1524" layer="91"/>
<junction x="76.2" y="27.94"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="63.5" y1="48.26" x2="88.9" y2="48.26" width="0.1524" layer="91"/>
<junction x="88.9" y="48.26"/>
<pinref part="S2" gate="G$1" pin="4"/>
<wire x1="63.5" y1="48.26" x2="63.5" y2="27.94" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="3"/>
<wire x1="63.5" y1="27.94" x2="60.96" y2="27.94" width="0.1524" layer="91"/>
<junction x="63.5" y="27.94"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="50.8" y1="58.42" x2="88.9" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="IC2" gate="G$1" pin="PB2"/>
<wire x1="88.9" y1="81.28" x2="88.9" y2="73.66" width="0.1524" layer="91"/>
<wire x1="88.9" y1="73.66" x2="60.96" y2="73.66" width="0.1524" layer="91"/>
<wire x1="60.96" y1="73.66" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<wire x1="88.9" y1="58.42" x2="88.9" y2="73.66" width="0.1524" layer="91"/>
<junction x="88.9" y="58.42"/>
<junction x="88.9" y="73.66"/>
<pinref part="X2" gate="G$1" pin="3"/>
<wire x1="60.96" y1="106.68" x2="60.96" y2="73.66" width="0.1524" layer="91"/>
<junction x="60.96" y="73.66"/>
<pinref part="S1" gate="G$1" pin="4"/>
<wire x1="50.8" y1="58.42" x2="50.8" y2="27.94" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="50.8" y1="27.94" x2="48.26" y2="27.94" width="0.1524" layer="91"/>
<junction x="50.8" y="27.94"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="PB1"/>
<pinref part="X2" gate="G$1" pin="2"/>
<wire x1="58.42" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="58.42" y1="106.68" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="DI"/>
<wire x1="93.98" y1="76.2" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<junction x="58.42" y="76.2"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="1"/>
<pinref part="IC2" gate="G$1" pin="PB0"/>
<wire x1="55.88" y1="106.68" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<wire x1="55.88" y1="78.74" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="6"/>
<pinref part="IC2" gate="G$1" pin="PB5"/>
<wire x1="68.58" y1="106.68" x2="68.58" y2="66.04" width="0.1524" layer="91"/>
<wire x1="68.58" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="236.22" y1="76.2" x2="246.38" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="DO"/>
<pinref part="LED2" gate="G$1" pin="DI"/>
<wire x1="119.38" y1="76.2" x2="127" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="DO"/>
<pinref part="LED3" gate="G$1" pin="DI"/>
<wire x1="152.4" y1="76.2" x2="160.02" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="DO"/>
<pinref part="LED4" gate="G$1" pin="DI"/>
<wire x1="185.42" y1="76.2" x2="193.04" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="S4" gate="G$1" pin="4"/>
<pinref part="S4" gate="G$1" pin="3"/>
<wire x1="86.36" y1="27.94" x2="88.9" y2="27.94" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<junction x="88.9" y="27.94"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="LED4" gate="G$1" pin="DO"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="218.44" y1="76.2" x2="226.06" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
