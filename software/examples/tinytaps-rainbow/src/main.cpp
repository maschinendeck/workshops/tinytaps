#include <Arduino.h>
#include "tinytaps.h"

// HSV Convert from https://gist.github.com/postspectacular/2a4a8db092011c6743a7

double intp;

float fract(float x) {
  return x - int(x);
}

float mix(float a, float b, float t) {
  return a + (b - a) * t;
}

float step(float e, float x) {
  return x < e ? 0.0 : 1.0;
}

float* hsv2rgb(float h, float s, float b, float* rgb) {
  rgb[0] = b * mix(1.0, constrain(abs(fract(h + 1.0) * 6.0 - 3.0) - 1.0, 0.0, 1.0), s);
  rgb[1] = b * mix(1.0, constrain(abs(fract(h + 0.6666666) * 6.0 - 3.0) - 1.0, 0.0, 1.0), s);
  rgb[2] = b * mix(1.0, constrain(abs(fract(h + 0.3333333) * 6.0 - 3.0) - 1.0, 0.0, 1.0), s);
  return rgb;
}

void setup() {
  init_tinytaps();
}

void loop() {

  // Calculate base hue
  double angle = (millis() / 1000.0f) / 4.0f;
  double hue = modf(angle, &intp);

  for (int led = 0; led < 4; led++) {
    // HSV to RGB
    float rgb[3];
    float hue_led = modf(hue + (led / 4.0f), &intp);
    hsv2rgb(hue_led, 1.0f, 1.0f, rgb);

    set_led(led, (int) (rgb[0] * 255),
                 (int) (rgb[1] * 255),
                 (int) (rgb[2] * 255));
  }

  delay(10);
}