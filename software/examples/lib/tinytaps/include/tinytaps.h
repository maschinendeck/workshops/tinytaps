#include "WS2812.h"
#include "EEPROM.h"

#ifndef TINYTAPS_H
#define TINYTAPS_H

#define TT_LEDS_COUNT 4
#define TT_LEDS_PIN PB1

#define COLOR_BLACK  0
#define COLOR_RED    1
#define COLOR_YELLOW 2
#define COLOR_GREEN  3
#define COLOR_BLUE   4
#define COLOR_WHITE  5
#define COLOR_GREY   6

extern WS2812 _tt_leds;             // Databuffer for WS2812
extern bool _tt_led_auto_update;    // Flag, indicates auto draw of ws2812 

void init_tinytaps();

void clear_leds();
void set_all_leds(int color);
void set_led(int led_nr, int color);
void set_led(int led_nr, uint8_t red, uint8_t green, uint8_t blue);
void update_leds();

#define BUTTON_PIN AIN1D
#define BUTTON_COUNT 4
#define LONG_PRESS_TIMER 2000
#define DEBOUNCE_TIMER 100 // debounce

class TTButtons
{
    public:
        TTButtons();
        void update();

        int shortPress();
        int longPress();
        int isPressed();
        boolean shortPress(int nr);
        boolean longPress(int nr);
        boolean isPressed(int nr);
        void reset();

    private:
        // current hardware doesn't allow multitouch
        int buttonPressed;
        unsigned long pressTime;
        int longPressFlag;
        int shortPressFlag;

        int tresholds[4];
};
#endif // TINYTAPS_H
