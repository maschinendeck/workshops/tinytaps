#include <Arduino.h>
#include "tinytaps.h"

// TinyTaps Button Objekt
TTButtons buttons; 

// schaltet die led mit der korrekten farbe ein
void display_simon_color(int nr)
{
    if (nr == 0)
    {
        set_led(0, COLOR_GREEN);
    } 
    else if (nr == 1)
    {
        set_led(1, COLOR_YELLOW);
    }
    else if (nr == 2)
    {
        set_led(2, COLOR_BLUE);
    }
    else if (nr == 3)
    {
        set_led(3, COLOR_RED);
    }
    else
    {
        clear_leds();
    }
}

// Wird einmal beim Starten ausgeführt
void setup() 
{
    // Initialisiert die TinyTaps Platine
    init_tinytaps();
}

// Wird danach als Schleife ausgeführt
void loop() 
{
    // Eine Liste von bis zu 30 Farben erstellen
    int colorList[30];

    // Aktuelles Level (0 - 29)
    int currentLevel = 0;

    // Variable, die angibt ob gerade ein spiel läuft
    int gameRunning = 1;

    // Diese schleife läuft solange, bis gameRunning = 0 ist
    while (gameRunning == 1)
    {
        // Füge eine neue zufällige Farbe an der stelle currentLevel der liste hinzu
        // random(4) gibt eine zufällige zahl zwischen 0 und 3 zurück
        colorList[currentLevel] = random(4);

        // nun alle bisherigen farben anzeigen
        int index = 0;
        while (index <= currentLevel) // solange index kleiner als der aktuelle level ist
        {
            // LED, die der farbe entspricht einschalten
            display_simon_color(colorList[index]);

            // 500 ms warten, dann LEDs wieder ausschalten
            delay(500);
            clear_leds();
            // nach dem ausschalten 100ms warten bevor die nächste farbe angezeigt wird
            delay(100);

            // index erhöhen um mit der nächsten farbe fortzufahren
            index = index + 1;
        }

        // Alle Farben wurden angezeigt. Der Nutzer soll nun die farben wiederholen
        // Wir müssen überprüfen, ob die korrekten farben gedrückt werden. Begonnen wird wieder bei farbe 1
        int prueffarbe = 0;
        while (prueffarbe <= currentLevel && gameRunning == 1)
        {
            // Warte auf Nutzereingabe
            int nutzereingabe = -1;
            // solange die nutzereingabe immer noch -1 ist (kein button gedrückt ist), buttons updaten
            while (nutzereingabe == -1)
            {
                // prüfen, ob der button kurz gedrückt wurde
                buttons.update();
                // short press gibt erst den button zurück, wenn er wieder losgelassen wurde
                // so werden keine buttons doppelt gezählt
                nutzereingabe = buttons.shortPress();
            }

            // überprüfen, ob die nutzereingabe der farbe entspricht
            if (nutzereingabe == colorList[prueffarbe])
            {
                // nutzer hat richtig geraten, zur bestätigung gedrückte farbe anzeigen
                display_simon_color(nutzereingabe);
                delay(250);
                clear_leds();

                // Falls die letzte Farbe des levels erfolgreich eingegeben wurde
                if (prueffarbe == currentLevel)
                {
                    set_all_leds(COLOR_GREEN);
                    delay(500);
                    clear_leds();
                }
            }
            else
            {
                // Der nutzer hat eine falsche farbe eingegeben: alle LEDs auf rot schalten und das spiel beenden
                set_all_leds(COLOR_RED);
                delay(2000);
                clear_leds();

                gameRunning = 0;
            }

            prueffarbe = prueffarbe + 1;
        }

        // falls das letzte level erreicht wurde, von vorne anfangen
        if (currentLevel == 29)
        {
            gameRunning = 0;
        }

        // ins nächste level aufsteigen
        currentLevel = currentLevel + 1;
    }






}