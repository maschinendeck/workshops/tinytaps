#include <Arduino.h>
#include "tinytaps.h"
#include "DigiKeyboardDe.h"

// TinyTaps button object
TTButtons buttons; 

// Farben und Passwörter
int colors[4];
const char *passwords[4];

// Auf true ändern, falls "Enter" nach Eingabe gedrückt werden soll
bool sendEnter = true;

// Wird einmal beim Starten ausgeführt
void setup() 
{
    // Initialisiert die TinyTaps Platine
    init_tinytaps();

    // Einige sekunden warten, bis PC den TinyTaps als tastatur erkannt hat
    DigiKeyboardDe.delay(2500);

    passwords[0] =  "https://gitlab.com/maschinendeck/workshops/tinytaps";
    passwords[1] =  "https://maschinendeck.org/";
    passwords[2] =  "password3";
    passwords[3] =  "passwort4";
    colors[0] =     COLOR_BLUE;
    colors[1] =     COLOR_GREEN;
    colors[2] =     COLOR_RED;
    colors[3] =     COLOR_YELLOW;

    // LED farben anzeigen (man kann sich die passwörter anhand der Farben merken)
    for (int i = 0; i < 4; i++)
    {
        set_led(i, colors[i]);
        delay(50);
    }
}

// Wird danach als Schleife ausgeführt
void loop() 
{
    buttons.update();
    int pressedButton = buttons.shortPress();

    if (pressedButton != -1)
    {
        // Benötigt für kompatiblität mit alten systemen
        DigiKeyboardDe.sendKeyStroke(0);

        // passwords[pressedButton]
        for (int i = 0; i < strlen(passwords[pressedButton]); i++)
        {
            DigiKeyboardDe.print(passwords[pressedButton][i]);
            DigiKeyboardDe.delay(20);
        }

        if (sendEnter)
            DigiKeyboardDe.sendKeyStroke(KEY_ENTER);
    }

    DigiKeyboardDe.delay(10);
}