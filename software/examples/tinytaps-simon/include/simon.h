/***********************************************
 * TinyTaps - Simon Says                       *
 *                                             *
 * Dies ist eine Implementierung des populären *
 * Spiels 'Simon Says' für den TinyTaps. Dise  * 
 * Version ist absichtlich einfach gehalten.   *
 *                                             *
 * Version 1.0                                 *
 * Author: IcePhoenix, Seze                    *
 ***********************************************/

#include <Arduino.h>
#include "tinytaps.h"

// Nach wie vielen Leveln der Spieler gewonnen hat
#define SIMON_MAX_LEVEL 36

// Zuordnung von Knopfnummern zu Farben
extern int SIMON_COLORS[4];

// Die Buttons über die das Spiel gesteuert wird
extern TTButtons buttons;

// Abfolge der Farben (wir brauchen maximal so viele wie level)
extern uint8_t sequence[SIMON_MAX_LEVEL];

// Aktuelles level des Spielers
extern int level;

void play_start_animation();
void reset_game();
void advance_level();
void display_sequence();
int wait_button_released();
bool check_player_input();
void blink_three_times(int color);
