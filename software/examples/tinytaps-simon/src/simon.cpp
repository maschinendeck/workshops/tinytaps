// Diese Datei beinhaltet Quellcode, der in 'simon.h' beschrieben wurde.
// Dieswird durch ein 'include' (einbinden) angezeigt.
// Für weitere Informationen zum Code kann man dort auch nachschauen.
#include "simon.h"

// Hier sind die Speicherpositionen die in simon.h deklariert wurden
TTButtons buttons;
uint8_t sequence[SIMON_MAX_LEVEL];
int level;

int SIMON_COLORS[] = {                         // Zuordnung von Knopf zu Farbe
  COLOR_GREEN,                                 // Knopf eins ist grün
  COLOR_YELLOW,                                // Knopf zwei ist gelb
  COLOR_BLUE,                                  // Knopf drei ist blue
  COLOR_RED                                    // Knops vier ist red
};

// Spiele eine Animation wenn der TinyTaps startet
void play_start_animation() {       
  set_led(0, SIMON_COLORS[0]);                 // Leuchte Knopf eins                
  delay(250);
  set_led(1, SIMON_COLORS[1]);                 // Leuchte Knopf zwei
  delay(250);
  set_led(2, SIMON_COLORS[2]);                 // Leuchte Knopf drei
  delay(250);
  set_led(3, SIMON_COLORS[3]);                 // Leuchte Knopf vier
  delay(250);
  clear_leds();                                // Alle LEDs ausschalten
  delay(250);
  set_all_leds(COLOR_WHITE);                   // Alle LEDs weis einschalten
  delay(250);
  clear_leds();                                // LEDs wieder ausschalten
  delay(250);
}

// Setze das Spiel auf den Anfang zurück
void reset_game() {
  level = 0;                                        // Das aktuelle level zurücksetzen
}

// Gehe in das nächste Level (wähle zufälligen zu drückenden knopf)
void advance_level() {
  int new_button = random(4);                       // Rolle ein 'vierseitigen' Würfel
  sequence[level] = new_button;                     // Speicher Ergebniss in der zu drückenden Sequenz
  level = level + 1;                                // Erhöhe das aktuelle level
}

// Zeige die Sequenz  der zu drückenden Knöpfe
void display_sequence() {
  for (int i = 0; i < level; i++) {                 // Laufe über alle Einträge in der sequenz
    int button_to_press = sequence[i];              // Finde den zu drückenden Knopf
    int color = SIMON_COLORS[button_to_press];      // Farbe für led nachschauen
    set_led(button_to_press, color);                // Zeige die Farbe an       
    delay(500);                                     // Warte kurz
    clear_leds();                                   // LEDs wieder ausschalten
    delay(300);                                     // Wieder kurz warten
  }
}

// Warte darauf das ein Knopf gedrückt und wieder losgelassen wurde
int wait_button_released() {
  while (true) {                                    // In einer endlosschleife

    // Anzeigen, dass ein Button gedrückt wurde
    buttons.update();                               // Lese den Zustand der Knöpfe
    int btn_nr = buttons.isPressed();               // Sehe nach ob einer gedrückt wurde
    if (btn_nr >= 0) {                              // Falls dies der Fall ist
      set_led(btn_nr, SIMON_COLORS[btn_nr]);        // Lasse die LED daneben aufleuchten
    } else {                                        // Ansonsten
      clear_leds();                                 // Schalte alle LEDs aus
    }

    // Falls Button losgelassen wurde, button zurückgeben
    int buttonReleased = buttons.shortPress();      // Sehe nach ob ein Knopf losgelassen wurde
    if (buttonReleased >= 0) {                      // Falls ja
      return buttonReleased;                        // Gebe die nummer des knopfes zurück
    }
    delay(10);                                      // Warte etwas
  }
}

// Frage die sequenz vom spieler ab, gibt wahr zurück wenn er ein Fehler gemacht hat
bool check_player_input() {
  for (int index = 0; index < level; index++) {     // Laufe über alle einträge in der Sequenz
    int button_pressed = wait_button_released();    // Warte darauf das ein Knopf gedrückt wird
    if (button_pressed != sequence[index]) {        // Ist der gedrückt knopf nicht der erwartete
      return true;                                  // Gebe wahr zurück (wahr = spieler hat ein fehler gemacht)
    }
  }                                                 // Ansonsten
  return false;                                     // Gebe falsch zurück (falsch = spieler hat kein fehler gemacht)
}

// Blinke drei mal kurz in folge die gegebene Farbe
void blink_three_times(int color) {
  for (int times = 0; times < 4; times++) {         // In einer Schleife, mache folgendes drei mal:
    set_all_leds(color);                            // Alle leds an mit der gegebenen Farbe
    delay(200);                                     // Kurz warten
    clear_leds();                                   // Alle leds wieder ausschalten
    delay(200);                                     // Wieder kurz warten
  }
}
