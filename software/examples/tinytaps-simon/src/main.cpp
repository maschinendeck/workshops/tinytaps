#include <Arduino.h>
#include "tinytaps.h"
#include "simon.h"

void setup() {
  init_tinytaps();
  play_start_animation();
  reset_game();
}

void loop() {
  
  advance_level();

  display_sequence();

  bool made_mistake = check_player_input();

  if (made_mistake) {
    reset_game();
    blink_three_times(COLOR_RED);
  }

  delay(500);

}