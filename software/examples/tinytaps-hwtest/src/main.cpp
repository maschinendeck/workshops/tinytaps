#include <Arduino.h>
#include "tinytaps.h"

TTButtons buttons; 

void setup() 
{   
    init_tinytaps();
}

void loop() 
{
    buttons.update();
    int bidx = buttons.isPressed();

    for (int i = 0; i < 4; i++)
    {
        if (bidx == i) 
        {
            set_led(i, COLOR_WHITE);
        }
        else 
        {
            set_led(i, COLOR_GREY);
        }
    }
}