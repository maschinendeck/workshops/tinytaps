#include <Arduino.h>
#include "tinytaps.h"

// Der Name des Button Pins findet man im Datenblatt des Mikrocontrollers
#define PIN_BUTTONS AIN1D

// TinyTaps Button Element
TTButtons buttons; 

void setup() 
{
    // Initialisiert die TinyTaps Platine
    init_tinytaps();

    // Konfiguriert den Pin der Buttons als Eingang
    pinMode(PIN_BUTTONS, INPUT);
}

void loop() 
{
    // Dieser befehl liest die Spannung am Button PIN und speichert ihn in "buttonWert" (zwischen 0 (0V) und 1023 (5V))
    int buttonWert = analogRead(PIN_BUTTONS);

    /* Button | Spannung | analogRead()  1023 geteilt durch 5V mal Spannung
     *      1 | 0V       | 0
     *      2 | 2.5V     | 511
     *      3 | 3.33V    | 681
     *      4 | 3.75V    | 767
     * keiner | 5V       | 1023
     * 
     * Da die Widerstände Toleranzen haben, müssen Bereiche für jeden Button angegeben werden.
     * Es darf keine Überschneidungen geben, da immer nur ein Button gleichzeitig gedrückt sein kann
     * Button | buttonWert von | buttonWert bis
     *      1 | 0              | 255
     *      2 | 256            | 596
     *      3 | 597            | 724
     *      4 | 725            | 895
     * keiner | 896            | 1023
     */

    // Mit "if" wird ein vergleich definiert
    if (buttonWert <= 255) // <= bedeutet "kleiner gleich"
    {   // Button 1 gedrückt

        // mit set_led kann jeder LED eine Farbe zugewiesen werden
        // In der Informatik wird dabei immer von 0 an gezählt, d.h. die erste LED ist LED 0, dann 1, ...
        set_led(0, COLOR_RED);
        set_led(1, COLOR_BLACK);
        set_led(2, COLOR_BLACK);
        set_led(3, COLOR_BLACK);
    }
    else if (buttonWert <= 596)  // falls der vergleich fehlschlägt (buttonWert größer oder gleich 256), dann wird die nächste bedingung überprüft
    {   // Button 2 gedrückt

        // da der erste Vergleich bereits fehlgeschlagen ist, muss hier nicht erneut geprüft werden, ob buttonWert >= 256 ist. 
        // Dies wird bereits durch "nicht kleiner gleich 255" aus dem ersten Vergleich sichergestellt                           
        set_led(0, COLOR_BLACK);
        set_led(1, COLOR_RED);
        set_led(2, COLOR_BLACK);
        set_led(3, COLOR_BLACK);      
    }
    else if (buttonWert <= 724)
    {   // Button 3 gedrückt
        set_led(0, COLOR_BLACK);
        set_led(1, COLOR_BLACK);
        set_led(2, COLOR_RED);
        set_led(3, COLOR_BLACK);  
    }
    else if (buttonWert <= 895)
    {   // Button 4 gedrückt
        set_led(0, COLOR_BLACK);
        set_led(1, COLOR_BLACK);
        set_led(2, COLOR_BLACK);
        set_led(3, COLOR_RED);  
    }
    else // falls keine der Bedingungen zutrifft
    {
        set_led(0, COLOR_BLACK);
        set_led(1, COLOR_BLACK);
        set_led(2, COLOR_BLACK);
        set_led(3, COLOR_BLACK);  
    }

    // Alternativ kann der gedrückte Button ebenfalls in eine Variable geschrieben werden.
    // Es ist oft eine gute Idee, einer Variablen einen Grundwert (hier -1) zu geben, damit sie einen definierten Zustand haben
    // Dieser kann durch weitere Zuweisungen überschrieben werden
    int buttonNummer = -1;
    if (buttonWert <= 255) 
    {
        buttonNummer = 0;
    } 
    else if (buttonWert <= 596)
    {
        buttonNummer = 1;
    }
    else if (buttonWert <= 724)
    {
        buttonNummer = 2;
    }
    else if (buttonWert <= 895)
    {
        buttonNummer = 3;
    }
    else
    {
        buttonNummer = -1;
    }

    // ButtonNummer enthält jetzt einen Wert von -1 (kein Button gedrückt) oder 1,2,3,4 (Button 1-4 gedrückt)
    // Damit lässt sich der Code um die LED Farben zu setzen auch wie folgt schreiben:
    if (buttonNummer == 1) // Die zwei == werden zum Vergleichen von Werten genutzt, da ein einzelnes = wie im Code weiter oben "buttonNummer" den Wert 1 zuweisen würde
    {
        // Button 2 ist gedrückt
    }
    else
    {
        // Button 2 ist nicht gedrückt
    }

    // Will man dies für alle LEDs tun, so nutzt man eine so genannte "for" Schleife
    for (int i = 0; i < 4; i++)
    {
        /* Die obere Zeile bedeutet folgendes:
            * - Der Wert "i" startet bei 1
            * - Die Schleife wird ausgeführt, solange i < 4 ist, also für i = 0,1,2,3
            * - Am Ende der Schleife wird i um 1 erhöht. "i++" ist die Kurzform für "i=i+1" 
        */ 

        // FÜR JEDE LED (i = 0,1,2,3) WIRD GEPRÜFT
        if (buttonNummer == i) // wenn der gedrückte button der LED nummer entspricht
        {
            set_led(i, COLOR_RED); // wird die Farbe der LED auf rot gesetzt
        }
        else
        {
            set_led(i, COLOR_BLACK); // andernfalls wird sie ausgeschaltet
        }
    }

    // Damit nicht immer die Button nummer von hand überprüft werden muss, kann die Funktion buttons.isPressed() genutzt werden
    // Dafür muss zuvor allerdings buttons.update() aufgerufen werden:
    buttons.update();
    buttonNummer = buttons.isPressed();
}