#include "tinytaps.h"

WS2812 _tt_leds(TT_LEDS_COUNT);
bool _tt_led_auto_update;

void init_tinytaps() {

    // Init LEDs
    _tt_leds.setOutput(TT_LEDS_PIN);
    _tt_led_auto_update = true;
    clear_leds();

    // Init RNG
    randomSeed(EEPROM.read(0x00) ^ analogRead(2));
    EEPROM.write(0x00, (uint8_t)random(256));


}

void set_led(int led_nr, uint8_t red, uint8_t green, uint8_t blue) {
    if (led_nr >= 0 && led_nr < TT_LEDS_COUNT) {
        cRGB color = {.g = green, .r = red, .b = blue};
        _tt_leds.set_crgb_at(led_nr, color);
    }
    if (_tt_led_auto_update) {
        _tt_leds.sync();
    }
}

void update_leds()
{
    _tt_leds.sync();
}

void set_led(int led_nr, int color) {
    switch (color) {   // Saves ram compared to LUT
        case COLOR_BLACK:  { set_led(led_nr,   0,   0,   0); }; break;
        case COLOR_RED:    { set_led(led_nr, 64,   0,   0); }; break;
        case COLOR_YELLOW: { set_led(led_nr, 64, 32,   0); }; break; // 255 appears very green
        case COLOR_GREEN:  { set_led(led_nr,   0, 64,   0); }; break;
        case COLOR_BLUE:   { set_led(led_nr,   0,   0, 64); }; break;
        case COLOR_WHITE:  { set_led(led_nr, 64, 64, 64); }; break;
        case COLOR_GREY:   { set_led(led_nr,  20,  20,  20); }; break;
        default: {}; break;
    }
}

void set_all_leds(int color) {
    _tt_led_auto_update = false;
    for (int i = 0; i < TT_LEDS_COUNT; i++) {
        set_led(i, color);
    }
    _tt_led_auto_update = true;
    _tt_leds.sync();
    delay(1);
    update_leds();
}

void clear_leds() {
    set_all_leds(COLOR_BLACK);
}


TTButtons::TTButtons()
{
    int curTreshold = 1023;
    int nextTreshold;

    pinMode(BUTTON_PIN, INPUT);

    for (int i = BUTTON_COUNT-1; i >= 0; i--)
    {
        nextTreshold = 1023 * (i) / (i+1);
        tresholds[i] = (curTreshold + nextTreshold) / 2;
        curTreshold = nextTreshold;
    }

    this->reset();
}

void TTButtons::update()
{
    int voltage = analogRead(BUTTON_PIN);
    int prevPressed = buttonPressed;
    unsigned long pressDuration;

    buttonPressed = -1;
    for (int i = 0; i < BUTTON_COUNT; i++)
    {
        if (voltage < tresholds[i])
        {
            buttonPressed = i;
            break;
        }
    }

    if (prevPressed != buttonPressed)
    {
        if (buttonPressed == -1) // prev. Button released
        {
            pressDuration = (millis() - pressTime);
            if (pressDuration > LONG_PRESS_TIMER)
            {
                longPressFlag = prevPressed;
                shortPressFlag = -1;
            }
            else if (pressDuration > DEBOUNCE_TIMER)
            {
                shortPressFlag = prevPressed;
            }
        }
        else // different button pressed
        {
            pressTime = millis();
        }
    }
}


int TTButtons::isPressed()
{
    return buttonPressed;
}

int TTButtons::shortPress()
{
    int tmp = shortPressFlag;
    shortPressFlag = -1;
    return tmp;
}

int TTButtons::longPress()
{
    int tmp = longPressFlag;
    longPressFlag = -1;
    return tmp;
}

boolean TTButtons::shortPress(int nr)
{
    return (this->shortPress() == nr);
}

boolean TTButtons::longPress(int nr)
{
    return (this->longPress() == nr);
}

boolean TTButtons::isPressed(int nr)
{
    return (buttonPressed == nr);
}


void TTButtons::reset()
{
    shortPressFlag = -1;
    longPressFlag = -1;
    buttonPressed = -1;
}
