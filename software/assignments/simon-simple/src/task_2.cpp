#include "assignment.h"

/*
Aufgabe 3:

Nun kümmern wir uns um das Einlesen der Eingaben des Benutzers und entscheiden,
ob dieser die richtige Sequenz eingeben hat. 

Aufgabe:
    Schreiben sie ein Programm welches vergleicht ob eine eingegebene Sequenz 
    der Vorgegebenen entspricht. Laufen sie über jede Farbe in der Sequenz.
    Warten sie auf eine Eingabe des Benutzers und vergleichen sie diese mit der 
    Erwartung (also der Farbe die gedrückt werden sollte, vorgegeben durch die 
    Sequenz). Ist sie gleich lassen sie zur Bestätigung kurz die Farbe aufleuchten.
    Ist sie anders (also Falsch) können sie die Weitere Überprüfung abbrechen.
    Geben sie am Ende eine 1 zurück wenn die Sequenz korrekt war, ansonsten
    geben sie eine 0 zurück.

Hinweis:

    1. So können sie auf eine Eingabe warten:
        int button = wait_button_press(buttons);

    2. So können sie die LED neben den Button zur Bestätigung leuchte lassen:
        acknowledge_input(button);

    3. In der while-Schleife können sie mit colorList[index] auf die erwartete
        Farbe aus der Sequenz zugreifen

    4. So überprüfen sie ob zwei Werte gleich sind:
        if (WERT1 == WERT2) {
            Anweisungen
        }

    5. So überprüfen sie ob zwei Werte ungleich sind:
        if (WERT1 != WERT2) {
            Anweisungen
        }

    6. So weisen sie einer Variablen ein neuen Wert zu:
        VARIABLE = NEUER WERT;
        z.B.: index = 42;

Erweiterung:
    Verwenden sie Anstelle von zwei if-statements ein if-else-statement

    Recherchieren sie andere Schleifentypen in C++ (z.B. for oder do-while)
    Schreiben sie ihr Programm so um, dass es eine For-Schleife verwendet

    Das Programm verwendet zum Signalisieren Korrektheit / Inkorrektheit numerische
    Werte (z.B. 0 für Falsch). Recherchieren sie Boolesche-Logik und schreiben sie
    ihr Programm so um, dass allCorrectUntilNow vom Typ bool ist. Verwenden sie
    die Wahrheitswerte true und false entsprechend. Ändern sie den Rückgabetyp 
    der Funktion.

    Verwenden sie ein Return-Statement in der Schleife um schon früher aus dieser
    Auszubrechen.

*/

int check_sequence(TTButtons& buttons, int* colorList, int currentLevel) {
    int allCorrectUntilNow = 1;
    int index = 0;

    // 1. Laufen sie über alle Farben in der vorgegebenen Sequenz (colorList)
    //    Hier müssen sie die Bedingung in der While-Schleife ändern
    //    Ersetzen sie 'false' gegen ein Bedingung, welche testet, ob index
    //    kleiner gleich currentLevel ist. Behalten sie den rest der Bedingung
    //    also '&& allCorrectUntilNow' unverändert bei.
    while (false && allCorrectUntilNow == 1) {

        // 2. Warten sie bis ein Button gedrückt wurde

        // 3. Gedrückter Button gleich Farbe in Sequenz?

        // 3.1 Sind diese gleich lassen sie die Farbe zur Bestätigung kurz aufleuchten

        // 3.2 Ansonsten ist die Eingabe falsch und sie können die Überprüfung abbrechen
        //     Setzen sie dazu allCorrectUntilNow auf 0

        index = index + 1;
    }

    return allCorrectUntilNow;
}

/*
Dies ist eine Hilfsfunktion welche darauf wartet, bis ein Button losgelassen
wird. Danach gibt sie die Nummer des Buttons zurück.
Hier müssen sie keine Änderungen vornehmen.
*/
int wait_button_press(TTButtons& buttons) {
    int nutzereingabe = -1; // Warte auf Nutzereingabe, -1 zeigt noch keine Eingabe an

    // Solange noch keine Eingabe passiert ist
    while (nutzereingabe == -1)
    {
        // Betrachte den Zustand der Buttons
        buttons.update();
        
        // Reagiere auf das loslassen eines Buttons 
        nutzereingabe = buttons.shortPress();
    }

    return nutzereingabe;
}

/*
Dies ist eine kleine Hilfsfunktion welche die übergebene Farbe kurz aufleuchten lässt.
Hier müssen sie keine Änderungen vornehmen.
*/
void acknowledge_input(int input) {
    display_simon_color(input);
    delay(250);
    clear_leds();
}
