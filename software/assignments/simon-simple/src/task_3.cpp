#include "assignment.h"

/*
Aufgabe 4:

Nun ist es an der Zeit auf die Eingabe des Benutzers zu reagieren.

Aufgabe(n):

    1. Ergänzen sie die Funktion decide_continue_game so, dass sie Animationen abspielt.
        War die Eingabe des Benutzers korrekt (angezeigt dadurch, dass passed == 1 ist) soll
        Erfolgs-Animation auf den LEDs angezeigt werden und das Spiel geht weiter (gameRunning = 1).
        War die Eingabe des Benutzers inkorrekt (angezeigt dadurch, dass passed == 0 ist) soll
        eine Misserfolgs-Animation auf den LEDs angezeigt werden und das Spiel neu gestartet werden.
        Dies können sie durch gameRunning = 0 erreichen.

    2. Ergänzen sie die Funktionen show_input_incorrect und show_input_correct so, dass
        sie dem Benutzer anzeigen ob er korrekt lag oder nicht. Die konkrete Animation ist
        dabei ihnen überlassen. Gerne können sie Erfolg durch kurzes Grünes leuchten anzeigen
        und Misserfolg durch langes Rotes leuchten.

Hinweis:

    1. Es ist schon Code vorgeben, diesen können sie wieder unverändert übernehmen

    2. In decide_continue_game sind Kommentare vorgegeben, die sie Anleiten

*/

void show_input_incorrect() {
    // Der Nutzer hat eine falsche Eingabe gemacht
}

void show_input_correct() {
    // Der Nutzer hat eine richtige Eingabe gemacht
}

int decide_continue_game(int passed, int currentLevel) {
    int gameRunning = 1;

    // Beende das spiel wenn letztes Level erreicht wurde
    if (currentLevel == 29) {
        gameRunning = 0;
    }

    // 1. Wurde die Sequenz richtig eingegeben?

    // 1.1 Ist sie korrekt rufen sie show_input_correct auf und setzen sie 
    //         gameRunning auf 1

    // 1.2 Ist sie inkorrekt rufen sie show_input_incorrect auf und setzen sie
    //         gameRunning auf 0

    return gameRunning;
}
