#pragma once

#include <Arduino.h>
#include "tinytaps.h"

// Aufgabe 1
// Programm zum Leuchten der LEDs
void display_simon_color(int number);

// Aufgabe 2
// Programm zum Erweitern und Anzeigen der Farbsequenz
void pick_new_color(int* colorList, int currentLevel);
void show_sequence(int* colorList, int currentLevel);

// Aufgabe 3
// Abfragen des Spielers
int check_sequence(TTButtons& buttons, int* colorList, int currentLevel);
int wait_button_press(TTButtons& buttons);
void acknowledge_input(int input);

// Aufgabe 4
// Reaktion auf die Eingabe des Spielers
int decide_continue_game(int passed, int currentLevel);
void show_input_incorrect();
void show_input_correct();
