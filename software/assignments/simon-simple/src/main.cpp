#include <Arduino.h>
#include "tinytaps.h"
#include "assignment.h"

// TinyTaps Button Objekt
TTButtons buttons; 

// Wird einmal beim Starten ausgeführt
void setup() 
{
    // Initialisiert die TinyTaps Platine
    init_tinytaps();

}

// Wird danach als endlose Schleife ausgeführt
void loop() 
{
    
    int colorList[30];      // Eine Liste von bis zu 30 Farben erstellen
    int currentLevel = 0;   // Aktuelles Level (0 - 29)
    int gameRunning = 1;    // Variable, die angibt ob gerade ein spiel läuft

    // Diese schleife läuft solange wie das spiel andauert
    while (gameRunning == 1)
    {

        // Wähle eine neue Farbe für die Sequenz
        pick_new_color(colorList, currentLevel);

        // Zeige die Sequenz dem Spieler
        show_sequence(colorList, currentLevel);

        // Überprüfe die eingegebene Sequenz
        int passed = check_sequence(buttons, colorList, currentLevel);

        // Entscheide ob das Spiel weitergehen soll
        gameRunning = decide_continue_game(passed, currentLevel);

        // Falls das spiel noch läuft, ein level aufsteigen
        if (gameRunning == 1) 
        {
            currentLevel = currentLevel + 1;
        }

    }

}
