#include "assignment.h"

/*
Aufgabe 2:

In Simon Says werden zu beginn des Spiels die zu drückenden Farben ausgegeben.
Für jede Farbe in der Sequenz wird die Farbe kurz Angezeigt, darauf folgt eine
kurze Dunkelperiode. In jedem Level im Spiel kommt eine Farbe zusätzlich in der 
Sequenz vor. Deswegen gibt es auch immer so viele Farben wie Level im Spiel. 
Beachten sie, dass Level ab 0 gezählt werden, also im Level 0 schon eine Farbe
in der Sequenz ist. 

Aufgabe:
    Schreiben sie ein Programm welches die Farbsequenz und das aktuelle Level
    als Eingabe bekommt. Laufen sie die Farben in der Sequenz ab zeigen sie 
    die Farbe für 500ms an und schalten sie danach die LEDs für weitere 100ms
    wieder ab. Überlegen sie sich dazu zuerst, wie sie über die Sequenz laufen
    und kümmern sie sich dann um die LED. 

Hinweis:

    1. Ein Teil des Code ist schon vorgegeben, sie können diesen Übernehmen und
    müssen ihn nicht ändern.

    2. Der Code unten beinhaltet eine Multiple-Choice-Frage, hier müssen sie nur
    die Lösung auskommentieren, von der sie Denken, dass sie die Richtige ist
    sowie die darunter stehende Anweisung löschen.

    3. In der While-Schleife können sie mit colorList[index] auf die aktuelle
    Farbe in der Sequenz zugreifen.

    4. Verwenden sie display_simon_color um die aktuelle Farbe in der Sequenz
    anzuzeigen.

    5. So halten sie das Programm für 1000ms (= 1 Sekunde) an:
    delay(1000);

*/

void pick_new_color(int* colorList, int currentLevel) {
        colorList[currentLevel] = random(4);
}

void show_sequence(int* colorList, int currentLevel) {

    // Dies ist eine Variable die Speichert an welcher Stelle in der Sequenz wir sind
    int index = 0;

    // Multiple-Choice-Frage:
    // Welche der Folgenden Anweisungen ist die Richtige für die Schleife?
    // Zur Erinnerung: wir wollen alle Farben in der Sequenz ablaufen. Die Sequenz
    //     ist so lang, wie das aktuelle Level
    // 1. while (index <= currentLevel)
    // 2. while (index == currentLevel)
    // 3. while (currywurst)
    while (false)
    {

        // Zeige die Farbe in der Sequenz an und warte 500ms
        //     Verwenden sie dazu die Funktion display_simon_color und
        //     übergeben sie die aktuelle Farbe als Argument 

        // Schalte alle LEDs wieder aus und warte 100ms

        // Index erhöhen um mit der nächsten Farbe fortzufahren
        index = index + 1;
    }

}
