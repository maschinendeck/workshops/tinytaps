#include "assignment.h"

/*
Aufgabe 1:

In Simon Says haben die LEDs neben den Buttons eine feste Farbe. Z.B. ist die 
LED neben dem ersten Button (LED1) immer Grün. Da wir im Verlauf des Programms
diese LED häufig einschalten werden, wollen wir eine kleines Programm schreiben,
welches dieses für uns übernimmt. Das Programm bekommt deine Nummer übergeben, die
eine Farbe repräsentiert. Mithilfe dieser wird dann entschieden, welche LED in
welcher Farbe aufleuchten soll. Ist die übergebene Nummer keine valide Farbe 
(>= 5, da Simon Says nur 4 Farben hat) sollen alle LEDs ausgeschaltet werden.
Die Zuordnung ist wie folgt:

    Eingabe: 0 - LED 0 in COLOR_GREEN
    Eingabe: 1 - LED 1 in COLOR_YELLOW
    Eingabe: 2 - LED 2 in COLOR_BLUE
    Eingabe: 3 - LED 3 in COLOR_RED
    Ansonsten  - Alle Ausschalten

Aufgabe:
    Schreiben sie ein Programm welches die Nummer einer Farbe als Eingabe 
    bekommt und die entsprechende LED in der vorgegebenen Farbe leuchten lässt.
    Sollte die übergebene Nummer nicht valide sein (>= 5) schalten sie alle
    LEDs ab. Verwenden sie die Tabelle oben als Referenz. Verwenden sie nur
    if-statements und keine else oder else-if statements.

Hinweis:

    1. So fragen sie ab, ob die Nummer gleich 3 ist (beachten sie das doppelte '='):
    if (nummer == 3) {
        Anweisungen
    }

    2. So fragen sie ab, ob die Nummer größer oder gleich 2 ist:
    if (nummer >= 2) {
        Anweisungen
    }

    3. So schalten sie eine LED ein:
    set_led(Nummer der LED, Farbe der LED);
    z.B. set_led(0, COLOR_GREEN);

    4. So schalten sie alle LEDs aus:
    clear_leds();

Erweiterung:
    Recherchieren sie die Verwendung von else-if und else statements. Schreiben
    sie ihren Code nun so um, dass er else-if und else statements verwendet.

*/

void display_simon_color(int number)
{
    
    // Wenn Nummer gleich 0 ist
    // LED mit Nummer 0 auf Grün setzen

    // ...

    // Wenn die Nummer nicht valide ist (>= 5) 
    // Alle LEDs ausschalten

}
