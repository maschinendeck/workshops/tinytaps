#include "assignment.h"

/**************************
 * Task 0 - Display Color *
 **************************/

/* void display_simon_color(int number) {
    if (number == 0) {
        set_led(0, COLOR_GREEN);
    } else if (number == 1) {
        set_led(1, COLOR_YELLOW);
    } else if (number == 2) {
        set_led(2, COLOR_BLUE);
    } else if (number == 3) {
        set_led(3, COLOR_RED);
    }  else {
        clear_leds();
    }
} */

/**************************
 * Task 1 - Show Sequence *
 **************************/

/* void show_sequence(int* colorList, int currentLevel) {

    // Dies ist eine Variable die Speichert an welcher Stelle in der Sequenz wir sind
    int index = 0;

    // Laufe über die Länge der Sequenz
    while (index <= currentLevel)
    {

        // Zeige die Farbe in der Sequenz an und warte 500ms
        display_simon_color(colorList[index]);
        delay(500);

        // Schalte alle LEDs wieder aus und warte 100ms
        clear_leds();
        delay(100);

        // Index erhöhen um mit der nächsten Farbe fortzufahren
        index = index + 1;
    }

} */

/***************************
 * Task 2 - Check Sequence *
 ***************************/

/* int check_sequence(TTButtons& buttons, int* colorList, int currentLevel) {
    int allCorrectUntilNow = 1;
    int index = 0;

    // Laufen über alle Farben in der vorgegebenen Sequenz
    while (index <= currentLevel && allCorrectUntilNow) {

        // Warten sie bis ein Button gedrückt wurde
        int input = wait_button_press(buttons);

        // Gedrückter Button gleich Farbe in Sequenz?
        if (input == colorList[index]) {
            acknowledge_input(input); // Farbe kurz leuchten lassen
        } else {
            allCorrectUntilNow = 0; // Iteration abbrechen
        }

        index = index + 1;
    }

    return allCorrectUntilNow;
} */

/**************************
 * Task 3 - Output Result *
 **************************/

/* void show_input_incorrect() {
    set_all_leds(COLOR_RED);
    delay(2000);
    clear_leds();
}

void show_input_correct() {
    set_all_leds(COLOR_GREEN);
    delay(200);
    clear_leds();
}

int decide_continue_game(int passed, int currentLevel) {
    int gameRunning = 1;

    // Beende das spiel wenn letztes Level erreicht wurde
    if (currentLevel == 29) {
        gameRunning = 0;
    }

    // Wurde die Sequenz richtig eingegeben?
    if (passed == 1) {
        show_input_correct();
        gameRunning = 1;
    } else {
        show_input_incorrect();
        gameRunning = 0;
    }

    return gameRunning;
} */