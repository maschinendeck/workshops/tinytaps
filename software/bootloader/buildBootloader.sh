#! /bin/bash
mkdir -p micronucleus/firmware/configuration/tinytaps/
cp -r micronucleus_cfg/* micronucleus/firmware/configuration/tinytaps/
cd micronucleus/firmware/
make clean
make CONFIG=tinytaps
cd ../..
cp micronucleus/firmware/main.hex bootloader.hex

# Note: you will need the gcc-avr and avr-libc package