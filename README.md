# TinyTaps

![image of a tinytaps board](documentation/images/tt_long.jpg)

TinyTaps ist noch in der Entwicklung und diese Seite im Aufbau; bitte entschuldigt alle Unannehmlichkeiten.

## Einleitung
TinyTaps ist eine Lernplatine, die Anfängern den Einstieg in die Welt der Elektronik und Programmierung erleichtert. Es besteht aus einem Attiny85-Chip mit USB-Anschluss, vier Tastern und vier WS2812-LEDs. Das Board eignet sich besonders für Workshops, in denen Anfänger das Löten und Programmieren lernen wollen.

TinyTaps ist nicht nur ein Lernboard für Einsteigerinnen und Einsteiger, sondern auch ein Community-Projekt. Auf Workshops und Hacker-Events bieten wir das Board zum Verkauf an, möchten aber auch anderen die Möglichkeit geben, TinyTaps selbst zu bauen. Dazu stellen wir die Gerber-Dateien für das Board kostenlos zum Download zur Verfügung.

Das gleiche gilt natürlich auch für die Software und die Tutorials, die ihr hier im Git-Repository findet. Falls ihr Fehler findet oder Anregungen habt, könnt ihr uns diese gerne hier mitteilen. Wir glauben an die Kraft der Community und möchten mit TinyTaps dazu beitragen, dass mehr Menschen Zugang zu Elektronik und Programmierung bekommen. Wir sind gespannt, welche Ideen und Projekte mit TinyTaps entstehen.

## Ressourcen
Eine Anleitung wie ihr TinyTaps verwendet, findet ihr unter [Loslegen](documentation/GettingStarted.md)  
Dokumentation zu Hardware und Software findet ihr unter [Dokumentation](documentation/README.md)  
