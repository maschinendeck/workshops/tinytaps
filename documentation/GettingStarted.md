## Intro ##
It is great that you are interested in our tinytaps project! This page will help you set up your board, your IDE, drivers and teach you how to get software on your TinyTaps board.

## Bootloader ##
If you bought your Tinytaps already assembled at an event or participated in one of our workshops, you can skip this step. 
However, if you soldered it on your own, you need to follow [this](bootloader.md) guide to install the bootloader on the hardware.

## Setting up the IDE ##
We recommend Visual Studio Code as IDE for working with the Tinytaps board, as all our sample projects are set up in VS Code with platform IO. You can find it [here](https://code.visualstudio.com/). If you need to use Arduino IDE, we recommend following the Micronucleus / Digispark documentation on how to set it up.

In addition to Visual Studio Code, you will need to install the PlatformIO extension. In the sidebar, click on extensions, search for PlatformIO and click install. It is probably a good idea to install the C/C++ extensions, too.
Afterwards, click File->Open Folder and browse one of the example folders (e.g. software/examples/tinytaps-simon).

## Installing the Micronucleus drivers ##

### Windows ###
On windows, you need to use [Zadig](https://zadig.akeo.ie/) to install libusb drivers for the micronucleus bootloader. We also included a version [in this repository](../software/bootloader/windows_driver_installer/zadig-2.9.exe). Open the application, click Device->Load Preset Device and browse the file software/bootloader/windows_driver_installer/micronucleus.cfg. Make sure the settings are the same as in the following picture:
![](../software/bootloader/windows_driver_installer/zadig_screenshot.gif)
Click on "Install Driver". There should be a message saying "The driver has been installed successfully" after a few seconds.


### Linux ###
On linux, you only need to adjust the permissions for accessing the USB port. The easiest way to do it is to just run the script software/udev/update_udev.sh

### MacOS ###
Sadly, there is no guide how to set up the project on MacOS yet.

## Uploading your first code ##
> **Note:** You might need to try out several USB ports on your computer, as some don't seem to work

First of all, you might need to try several USB ports on your computer or USB hubs, because some don't seem to work. 
After opening the example project in Visual Studio Code, select the PlatformIO icon in the left sidebar. Click on "Upload" and wait until you are asked in the terminal to plug in your device. 
Now, press and hold the button S1 on the Tinytaps board and connect the USB to your computer. The software should start uploading and the terminal should show "success" afterwards. If not, try a different USB port.

## Further Information ## 
The micronucleus bootloader is used in a similar configuration in several other projects, most notably the famous digispark board. If you experience any trouble, you might find existing solutions for the digispark hardware, which also should work for tinytaps.