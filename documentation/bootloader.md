# Bootloader Setup
The tinytaps project comes uses the [Micronucleus](https://github.com/micronucleus/micronucleus) bootloader. Currently, version 2.2 is used, though we are planning on updating to the most recent version in the future. This guide will show you how to install the bootloader.

## Preparing Arduino Nano as an ISP
We recommend using an Arduino Nano as ISP for flashing the bootloader to the TinyTaps hardware. 
In future, we will provide a schematic on how to connect the Arduino nano board to the tinytaps hardware here.

## Flash bootloader using avrdudess
There are currently 2 different bootloader options available: 
|Version|Description|
|--|--|
|bootloader.hex|Only the bare bootloader - this will have the fastest upload time|
|bootloaderWithHwTest.hex|Bootloader bundled with hardware test software - all buttons should be white and light up when the corresponding button is pressed|